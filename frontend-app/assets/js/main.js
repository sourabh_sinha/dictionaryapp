function getPageResponse(){
	var urlValue = document.getElementById("urlInput").value;
	if (urlValue.trim().length){
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function(){
			if (this.readyState == 4 && this.status == 200) {
				var jsonResponse = JSON.parse(this.responseText);
				if (jsonResponse.RESULTS == "-1") {
					displayLoadingGifAndRepoll();
				}
				else 
					if (jsonResponse.results == "-2") {
						displayWordNotFound()
					}
					else {
						displayWordMeanings(jsonResponse);
					}
			}
		};
		
		xhttp.open("GET", "/frontend-app/processRequest.cfc?method=getPageResponse&url=" + urlValue, true);
		xhttp.send();
	}else{
		document.getElementById("word_meanings").innerHTML = "<p style='color:red'> Please provide valid input</p>"
	}
}
function displayLoadingGifAndRepoll(){
	document.getElementById("word_meanings").innerHTML ="<h3>Fetching Resources.Please Wait....</h3>";
	setTimeout(getPageResponse,"3000");
	document.getElementById("word_id").innerText = "";
	document.getElementById("urlInput").readOnly = true;
	document.getElementById("phoneticSpelling").innerText = "";
	document.getElementById("wordAudioFile").src = "";
}			
function displayWordNotFound(){
	document.getElementById("word_meanings").innerHTML = "<h3>The word " + document.getElementById("urlInput").value + " was not found in the dictionary.</h3>";
	document.getElementById("urlInput").readOnly = false;
	
}
function displayWordMeanings(resp){
	var wordMeaningDiv = document.getElementById("word_meanings");
	var finalElement = "";
	var pronunciations = resp.results[0].lexicalEntries[0].pronunciations[0];
	document.getElementById("urlInput").readOnly = false;
	document.getElementById("word_id").innerText = resp.results[0].word;
	document.getElementById("phoneticSpelling").innerText = "[" + pronunciations.phoneticSpelling + "]";
	document.getElementById("wordAudioFile").src = pronunciations.audioFile;
	document.getElementById("wordAudioFile").style.hidden = "initial";		
	for(wordResult of resp.results){
		for(lexicalEntry of wordResult.lexicalEntries){
			finalElement += "<div id='lexicalCategory_" + lexicalEntry.lexicalCategory + "' style='margin-top:10px'>" 
							+	"<span><b><i>" + lexicalEntry.lexicalCategory + "</i></b></span><br>" 
							;
			for(wordEntries of lexicalEntry.entries){							
				for(wordSense of wordEntries.senses){
					if(wordSense.definitions != undefined){
						for(wordDef of wordSense.definitions){
							finalElement += "<span class='wordDef'>" + wordDef + "</span>";
						}
					}
					if(wordSense.domains != undefined){
						for(wordDomains of wordSense.domains){
							finalElement += "<span class='wordDomains'> [" + wordDomains + "]</span>";
						}
					}else{
						finalElement += "<br>"; 
					}
					if(wordSense.examples != undefined){
						finalElement += "<ol>Examples"
						for(wordExamples of wordSense.examples){
							finalElement += "<li class='wordExamples'>" + wordExamples.text + "</li>";
						}	
						finalElement += "</ol>"
					}
				}
			}
		}
		finalElement += "</div>"
	}
	wordMeaningDiv.innerHTML = finalElement;	
}
