<html>
	<head>
		<title>Dictionary App</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		
		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
		
		<script src="assets/js/main.js" ></script>
	</head>
	<body>
		<div class="container mt-3">

			<h1>One Dictionary App</h1>
			<div class="input-group mb-1">
			  <input type="text" class="form-control" id="urlInput"  placeholder="Search">
			  <div class="input-group-append">
			    <button class="btn btn-primary" type="submit" onclick="getPageResponse()">Go</button>
			  </div>
			</div>
			
			<div id="page">
				<div class="word_metadata" style="margin-top:20px">
					<span id="word_id" style="font-size:xx-large"></span>
					<span id="phoneticSpelling"></span>
					<audio style="display:none" id="wordAudioFile" controls src="" type="audio/mp3"></audio>
				</div>
				<div id="word_meanings">
					
				</div>
				
			</div>
			 <footer style="margin-top:40%">
				<img src="https://developer.oxforddictionaries.com/images/PBO_blue.png" >
				<img width="500" height="75" src="https://helpx.adobe.com/content/dam/help/mnemonics/cf_2016_appicon_RGB.svg">
			</footer>
		</div>
		
	</body>
</html>