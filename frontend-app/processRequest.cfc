﻿component 
{
	remote boolean function addPageRequestToQueue(string url){
		writeLog(text="Add value to request queue" , log="console");
		
		var packet = {};
		packet.status = "SEND";
		packet.queue = "dynamicQueues/DEV.PageRequest";
		packet.id = #GenerateSecretKey("AES" )#;
		packet.msgid = "#Hash(arguments.url)#";
		packet.message = StructNew();
		packet.message.kind ="request" ;
		packet.message.urlValue = arguments.url;
		if(sendGatewayMessage("TestActiveMQ",packet) is "OK"){
			return true;	
		}
	}
	public boolean function addPageResponseToCache(struct data){
		//writeLog(text="add value to cache" , log="console");
		//writeDump(var="#arguments.data#" , output="E:\dump\dumpfile.html" , format="html");
		cachePut(Hash(arguments.data.msg.URLVALUE), data.msg.URLRESPONSE);
		
		return true;
	}
	remote struct function getPageResponse(string url) returnformat="JSON"   {
		writeLog(text="Get value from cache" , log="console");
		var pageResponse = cacheGet(Hash(arguments.url));
		if(isDefined("pageResponse") and Len(pageResponse)){
			return deserializeJSON(pageResponse);
		}else{
			addPageRequestToQueue(arguments.url);
			return {results = "-1"};
		}
	}
}