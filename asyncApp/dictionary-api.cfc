﻿component 
{
	this.oxfordAppId = 'f9599324';
	this.oxfordAppKey = 'd46e34277a599b9c666a0462f2c25ad1';
	public string function oxfordApiRequest(string word){
		var url = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/en/' & arguments.word;
		var httpService = new http();
		httpService.setMethod('GET');
		httpService.setUrl(url);
		httpService.addParam(type="header" , name = "app_id" , value=this.oxfordAppId);
		httpService.addParam(type="header" , name = "app_key" , value=this.oxfordAppKey); 
		var result = httpService.send().getPrefix();
		if(result.Responseheader.Status_Code EQ 404){
			return "{""results"":""-2""}";
		}
		return result.Filecontent;
	}
	public boolean function validateAndFormatInput(word){
		if(Len(word)){
			word = replace(word," ", "_" , "all");
		}
		return false;
	}	
}

