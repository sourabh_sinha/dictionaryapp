﻿component 
{
	public boolean function addPageResponseToQueue(string responseData, string urlValue){
		writeDump(var="#arguments#" , output="E:\dump\dumpfile.html" , format="html");
	
		var packet = {};
		packet.status = "SEND";
		packet.queue = "dynamicQueues/DEV.PageResponse";
		packet.id = #GenerateSecretKey("AES" )#;
		packet.msgid = "#Hash(arguments.urlValue)#";
		packet.message = {kind="response" , urlValue=arguments.urlValue , urlResponse = arguments.responseData};
		if(sendGatewayMessage("TestActiveMQ",packet) is "OK"){
			return true;	
		}
	}

	public string function sendRequests(string url){
		writeLog(text="get request" , log="console");
		var dictionaryApiObj = createObject("component","dictionary-api");
		//Add validation and lower case and replace scpaces with underscore
		return dictionaryApiObj.oxfordApiRequest(url);
		 
	}
}